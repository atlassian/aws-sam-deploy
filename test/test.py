import os
import shutil
import subprocess

import boto3
import pytest
import tomli
import tomli_w

from bitbucket_pipes_toolkit.test import PipeTestCase


AWS_DEFAULT_REGION = 'us-east-1'
AWS_SAM_TEST_STACK_NAME_S3 = f"sam-deploy-test-infra-sam-s3-{os.getenv('BITBUCKET_BUILD_NUMBER')}"
AWS_SAM_TEST_STACK_NAME_ECR = f"sam-deploy-test-infra-sam-ecr-{os.getenv('BITBUCKET_BUILD_NUMBER')}"
S3_BUCKET = f"bbci-pipes-sam-deploy-test-infra-{os.getenv('BITBUCKET_BUILD_NUMBER')}"
REPOSITORY_ECR = f"test/bbci-pipes-sam-deploy-test-infra-{os.getenv('BITBUCKET_BUILD_NUMBER')}"


class SamDeployTestCase(PipeTestCase):

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.aws_region = os.getenv('AWS_DEFAULT_REGION', AWS_DEFAULT_REGION)
        cls.aws_stack_name = AWS_SAM_TEST_STACK_NAME_S3
        cls.init_sam_app()
        cls.edit_samconfig()

    @classmethod
    def tearDownClass(cls):
        super().tearDownClass()
        shutil.rmtree('sam-app', ignore_errors=True)

    @pytest.mark.run(order=1)
    @staticmethod
    def init_sam_app():
        args = ('sam init --name sam-app --runtime python3.8 --app-template hello-world '
                '--dependency-manager pip --no-interactive')
        subprocess.run(args.split())

    @pytest.mark.run(order=2)
    @staticmethod
    def edit_samconfig():
        with open(f"{os.getcwd()}/sam-app/samconfig.toml", "rb") as f:
            toml_dict = tomli.load(f)

        # Disable resolve_s3 parameter
        toml_dict['default']['deploy']['parameters']['resolve_s3'] = False
        toml_dict['default']['package']['parameters']['resolve_s3'] = False

        with open(f"{os.getcwd()}/sam-app/samconfig.toml", "wb") as f:
            tomli_w.dump(toml_dict, f)

    def test_fail_if_no_params(self):
        result = self.run_container()
        self.assertRegex(result, '✖ Validation errors')

    def test_package_fail_if_template_not_exist(self):
        result = self.run_container(
            environment={
                'AWS_SECRET_ACCESS_KEY': os.getenv('AWS_SECRET_ACCESS_KEY'),
                'AWS_ACCESS_KEY_ID': os.getenv('AWS_ACCESS_KEY_ID'),
                'AWS_DEFAULT_REGION': self.aws_region,
                'COMMAND': 'package',
                'EXTRA_OPTIONS_PACKAGE_0': 's3_bucket_not_exist_0001',
                'EXTRA_OPTIONS_PACKAGE_1': '--template=template_not_exists.yaml',
                'EXTRA_OPTIONS_PACKAGE_COUNT': 2,
                'SAM_CLI_TELEMETRY': 0
            }
        )
        self.assertRegex(result, '✖ Pipe has finished with an error')

    def test_package_fail_if_s3_bucket_not_exist(self):
        result = self.run_container(
            environment={
                'AWS_SECRET_ACCESS_KEY': os.getenv('AWS_SECRET_ACCESS_KEY'),
                'AWS_ACCESS_KEY_ID': os.getenv('AWS_ACCESS_KEY_ID'),
                'AWS_DEFAULT_REGION': self.aws_region,
                'COMMAND': 'package',
                'EXTRA_OPTIONS_PACKAGE_0': 's3_bucket_not_exist_0001',
                'EXTRA_OPTIONS_PACKAGE_1': '--template=sam-app/template.yaml',
                'EXTRA_OPTIONS_PACKAGE_COUNT': 2,
                'SAM_CLI_TELEMETRY': 0
            }
        )
        self.assertRegex(result, '✖ Pipe has finished with an error')

    def test_package_only_success(self):
        result = self.run_container(
            environment={
                'AWS_SECRET_ACCESS_KEY': os.getenv('AWS_SECRET_ACCESS_KEY'),
                'AWS_ACCESS_KEY_ID': os.getenv('AWS_ACCESS_KEY_ID'),
                'AWS_DEFAULT_REGION': self.aws_region,
                'COMMAND': 'package',
                'EXTRA_OPTIONS_PACKAGE_0': f'--s3-bucket={S3_BUCKET}',
                'EXTRA_OPTIONS_PACKAGE_1': '--template=sam-app/template.yaml',
                'EXTRA_OPTIONS_PACKAGE_COUNT': 2,
                'SAM_CLI_TELEMETRY': 0
            }
        )
        self.assertIn('Using default authentication with AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY.', result)
        self.assertRegex(result, '✔ Pipe has finished successfully.')

    def test_oidc_successful(self):
        result = self.run_container(environment={
            'AWS_OIDC_ROLE_ARN': os.getenv('AWS_OIDC_ROLE_ARN'),
            'BITBUCKET_STEP_OIDC_TOKEN': os.getenv('BITBUCKET_STEP_OIDC_TOKEN'),
            'AWS_DEFAULT_REGION': os.getenv('AWS_DEFAULT_REGION', AWS_DEFAULT_REGION),
            'EXTRA_OPTIONS_DEPLOY_0': f'--s3-bucket={S3_BUCKET}',
            'EXTRA_OPTIONS_DEPLOY_1': f'--stack-name={self.aws_stack_name}',
            'EXTRA_OPTIONS_DEPLOY_2': '--capabilities',
            'EXTRA_OPTIONS_DEPLOY_3': 'CAPABILITY_IAM',
            'EXTRA_OPTIONS_DEPLOY_4': 'CAPABILITY_AUTO_EXPAND',
            'EXTRA_OPTIONS_DEPLOY_5': '--template=sam-app/template.yaml',
            'EXTRA_OPTIONS_DEPLOY_COUNT': 6,
            'SAM_CLI_TELEMETRY': 0
        })
        self.assertIn('Authenticating with a OpenID Connect (OIDC) Web Identity Provider.', result)
        self.assertRegex(result, '✔ Pipe has finished successfully.')

    def test_package_with_non_existing_sam_config(self):
        result = self.run_container(environment={
            'AWS_OIDC_ROLE_ARN': os.getenv('AWS_OIDC_ROLE_ARN'),
            'BITBUCKET_STEP_OIDC_TOKEN': os.getenv('BITBUCKET_STEP_OIDC_TOKEN'),
            'AWS_DEFAULT_REGION': os.getenv('AWS_DEFAULT_REGION', AWS_DEFAULT_REGION),
            'COMMAND': 'package',
            'EXTRA_OPTIONS_PACKAGE_0': f'--s3-bucket={S3_BUCKET}',
            'EXTRA_OPTIONS_PACKAGE_1': '--template=sam-app/template.yaml',
            'EXTRA_OPTIONS_PACKAGE_2': '--config-file=samconfig_not_exist.toml',
            'EXTRA_OPTIONS_PACKAGE_COUNT': 3,
            'SAM_CLI_TELEMETRY': 0
        })
        self.assertRegex(result, 'Pipe has finished with an error')

    @pytest.mark.run(order=-2)
    def test_deploy_successful(self):
        result = self.run_container(
            environment={
                'AWS_SECRET_ACCESS_KEY': os.getenv('AWS_SECRET_ACCESS_KEY'),
                'AWS_ACCESS_KEY_ID': os.getenv('AWS_ACCESS_KEY_ID'),
                'AWS_DEFAULT_REGION': self.aws_region,
                'EXTRA_OPTIONS_DEPLOY_0': f'--s3-bucket={S3_BUCKET}',
                'EXTRA_OPTIONS_DEPLOY_1': f'--stack-name={self.aws_stack_name}',
                'EXTRA_OPTIONS_DEPLOY_2': '--capabilities',
                'EXTRA_OPTIONS_DEPLOY_3': 'CAPABILITY_IAM',
                'EXTRA_OPTIONS_DEPLOY_4': 'CAPABILITY_AUTO_EXPAND',
                'EXTRA_OPTIONS_DEPLOY_5': '--template=sam-app/template.yaml',
                'EXTRA_OPTIONS_DEPLOY_COUNT': 6,
                'SAM_CLI_TELEMETRY': 0
            }
        )
        self.assertRegex(result, '✔ Pipe has finished successfully.')

    @pytest.mark.run(order=-1)
    def test_package_with_sam_config(self):
        sam_prefix_folder = 'config-s3-prefix'
        sam_config_template = f"""
        version=0.1
        [default.package.parameters]
        s3_prefix="{sam_prefix_folder}"
        """
        sam_config_file = 'config.toml'
        with open(sam_config_file, 'w') as f:
            f.write(sam_config_template)

        # Note! SAM CLI will start looking for a config file from root dir,
        # which is the directory where your template file presented.
        result = self.run_container(environment={
            'AWS_OIDC_ROLE_ARN': os.getenv('AWS_OIDC_ROLE_ARN'),
            'BITBUCKET_STEP_OIDC_TOKEN': os.getenv('BITBUCKET_STEP_OIDC_TOKEN'),
            'AWS_ACCESS_KEY_ID': os.getenv('AWS_ACCESS_KEY_ID'),
            'AWS_DEFAULT_REGION': os.getenv('AWS_DEFAULT_REGION', AWS_DEFAULT_REGION),
            'COMMAND': 'package',
            'EXTRA_OPTIONS_PACKAGE_0': f'--s3-bucket={S3_BUCKET}',
            'EXTRA_OPTIONS_PACKAGE_1': '--template=sam-app/template.yaml',
            'EXTRA_OPTIONS_PACKAGE_2': '--config-file=../config.toml',  # see notes
            'EXTRA_OPTIONS_PACKAGE_COUNT': 3,
            'SAM_CLI_TELEMETRY': 0,
            'DEBUG': True
        })
        self.assertIn('Authenticating with a OpenID Connect (OIDC) Web Identity Provider.', result)
        self.assertRegex(result, '✔ Pipe has finished successfully.')

        s3 = boto3.client('s3', region_name=os.getenv('AWS_DEFAULT_REGION'))
        content_packaged = s3.list_objects(Bucket=S3_BUCKET, Prefix=sam_prefix_folder)

        self.assertEqual(1, len(content_packaged['Contents']))
        self.assertRegex(content_packaged['Contents'][0]['Key'], rf'{sam_prefix_folder}\/[a-zA-Z0-9]*$')

        os.remove(sam_config_file)


class SamDeployImageTestCase(PipeTestCase):

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.aws_region = os.getenv('AWS_DEFAULT_REGION', AWS_DEFAULT_REGION)
        cls.aws_stack_name = AWS_SAM_TEST_STACK_NAME_ECR
        cls.init_sam_app()

    @classmethod
    def tearDownClass(cls):
        super().tearDownClass()
        shutil.rmtree('sam-app', ignore_errors=True)

    @pytest.mark.run(order=1)
    @staticmethod
    def init_sam_app():
        args_init = 'sam init --no-interactive --package-type Image --base-image amazon/python3.8-base ' \
                    '--dependency-manager pip --name sam-app --app-template hello-world-lambda-image'
        subprocess.run(args_init.split())

        args_build = 'sam build --template sam-app/template.yaml'
        subprocess.run(args_build.split())

    def test_deploy_failed(self):
        result = self.run_container(
            environment={
                'AWS_SECRET_ACCESS_KEY': os.getenv('AWS_SECRET_ACCESS_KEY'),
                'AWS_ACCESS_KEY_ID': os.getenv('AWS_ACCESS_KEY_ID'),
                'AWS_DEFAULT_REGION': self.aws_region,
                'EXTRA_OPTIONS_DEPLOY_0': f'--image-repository=5555555555.dkr.ecr.us-east-1.amazonaws.com/{REPOSITORY_ECR}',
                'EXTRA_OPTIONS_DEPLOY_1': f'--stack-name={self.aws_stack_name}',
                'EXTRA_OPTIONS_DEPLOY_2': '--capabilities',
                'EXTRA_OPTIONS_DEPLOY_3': 'CAPABILITY_IAM',
                'EXTRA_OPTIONS_DEPLOY_4': 'CAPABILITY_AUTO_EXPAND',
                'EXTRA_OPTIONS_DEPLOY_5': '--template=.aws-sam/build/template.yaml',
                'EXTRA_OPTIONS_DEPLOY_COUNT': 6,
                'SAM_CLI_TELEMETRY': 0,
                "DOCKER_HOST": "tcp://host.docker.internal:2375"
            },
            extra_hosts={"host.docker.internal": os.getenv('BITBUCKET_DOCKER_HOST_INTERNAL')}
        )
        self.assertRegex(result, '✖ Pipe has finished with an error')

    def test_deploy_successful(self):
        result = self.run_container(
            environment={
                'AWS_SECRET_ACCESS_KEY': os.getenv('AWS_SECRET_ACCESS_KEY'),
                'AWS_ACCESS_KEY_ID': os.getenv('AWS_ACCESS_KEY_ID'),
                'AWS_DEFAULT_REGION': self.aws_region,
                'EXTRA_OPTIONS_DEPLOY_0': f'--image-repository={os.getenv("AWS_ACCOUNT_NAME")}/{REPOSITORY_ECR}',
                'EXTRA_OPTIONS_DEPLOY_1': f'--stack-name={self.aws_stack_name}',
                'EXTRA_OPTIONS_DEPLOY_2': '--capabilities',
                'EXTRA_OPTIONS_DEPLOY_3': 'CAPABILITY_IAM',
                'EXTRA_OPTIONS_DEPLOY_4': 'CAPABILITY_AUTO_EXPAND',
                'EXTRA_OPTIONS_DEPLOY_5': '--template=.aws-sam/build/template.yaml',
                'EXTRA_OPTIONS_DEPLOY_COUNT': 6,
                'SAM_CLI_TELEMETRY': 0,
                "DOCKER_HOST": "tcp://host.docker.internal:2375"
            },
            extra_hosts={"host.docker.internal": os.getenv('BITBUCKET_DOCKER_HOST_INTERNAL')}
        )
        self.assertRegex(result, '✔ Pipe has finished successfully.')
