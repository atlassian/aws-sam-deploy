import configparser
from contextlib import contextmanager
from copy import copy
import importlib
import io
import os
import shutil
import sys
from unittest import mock, TestCase
import yaml


@contextmanager
def capture_output():
    standard_out = sys.stdout
    try:
        stdout = io.StringIO()
        sys.stdout = stdout
        yield stdout
    finally:
        sys.stdout = standard_out
        sys.stdout.flush()


class SamDeployTestCase(TestCase):

    def setUp(self):
        self.sys_path = copy(sys.path)
        sys.path.insert(0, os.getcwd())

    def tearDown(self):
        sys.path = self.sys_path

    @mock.patch.dict(os.environ, {
        'COMMAND': 'package',
        'AWS_ACCESS_KEY_ID': 'akiafkae',
        'AWS_SECRET_ACCESS_KEY': 'secretkey',
        'AWS_DEFAULT_REGION': 'test-region',
        'AWS_OIDC_ROLE_ARN': ''})
    @mock.patch('subprocess.run')
    def test_aws_sam_deploy_package_only(self, mock_subprocess):
        mock_subprocess.return_value = mock.Mock(returncode=0)
        current_pipe_module = importlib.import_module('pipe.pipe')

        with open(os.path.join(os.getcwd(), 'pipe.yml')) as f:
            metadata = yaml.safe_load(f.read())
        sam_deploy_pipe = current_pipe_module.SamDeployPipe(schema=current_pipe_module.schema,
                                                            pipe_metadata=metadata, check_for_newer_version=True)
        with capture_output() as out:
            sam_deploy_pipe.run()

        mock_subprocess.assert_called_once_with(
            ['sam', 'package'],
            check=True,
            capture_output=True,
            encoding=mock.ANY,
            text=True)
        self.assertRegex(out.getvalue(), '✔ Pipe has finished successfully.')

    @mock.patch.dict(os.environ, {
        'COMMAND': 'deploy',
        'AWS_ACCESS_KEY_ID': 'akiafkae',
        'AWS_SECRET_ACCESS_KEY': 'secretkey',
        'AWS_DEFAULT_REGION': 'test-region',
        'AWS_OIDC_ROLE_ARN': ''})
    @mock.patch('subprocess.run')
    def test_aws_sam_deploy(self, mock_subprocess):
        mock_subprocess.return_value = mock.Mock(returncode=0)
        current_pipe_module = importlib.import_module('pipe.pipe')

        with open(os.path.join(os.getcwd(), 'pipe.yml')) as f:
            metadata = yaml.safe_load(f.read())
        sam_deploy_pipe = current_pipe_module.SamDeployPipe(schema=current_pipe_module.schema,
                                                            pipe_metadata=metadata, check_for_newer_version=True)
        with capture_output() as out:
            sam_deploy_pipe.run()

        mock_subprocess.assert_called_once_with(
            ['sam', 'deploy', '--no-confirm-changeset', '--no-fail-on-empty-changeset'],
            check=True,
            capture_output=True,
            encoding=mock.ANY,
            text=True)
        self.assertRegex(out.getvalue(), '✔ Pipe has finished successfully.')


class SamDeployOidcMethodsTestCase(TestCase):

    def setUp(self):
        self.sys_path = copy(sys.path)
        sys.path.insert(0, os.getcwd())

    def tearDown(self):
        sys.path = self.sys_path
        test_dir = os.path.join(os.environ["HOME"], '.aws')
        if os.path.exists(test_dir):
            shutil.rmtree(test_dir)

    @mock.patch('pipe.pipe.logger.info')
    @mock.patch.dict(os.environ, {'AWS_ACCESS_KEY_ID': 'akiafkae', 'AWS_SECRET_ACCESS_KEY': 'secretkey',
                                  'AWS_DEFAULT_REGION': 'test-region', 'AWS_OIDC_ROLE_ARN': ''})
    def test_discover_auth_method_default_credentials_only(self, mock_logger):
        from pipe import pipe

        with open(os.path.join(os.getcwd(), 'pipe.yml')) as f:
            metadata = yaml.safe_load(f.read())
        pipe = pipe.SamDeployPipe(schema=pipe.schema, pipe_metadata=metadata, check_for_newer_version=True)
        pipe.auth()

        mock_logger.assert_called_with('Using default authentication with '
                                       'AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY.')
        self.assertEqual(pipe.auth_method, 'DEFAULT_AUTH')
        base_config_dir = f'{os.environ["HOME"]}/.aws'

        self.assertFalse(os.path.exists(f'{base_config_dir}/.aws-oidc'))

    @mock.patch('pipe.pipe.logger.info')
    @mock.patch.dict(os.environ, {'BITBUCKET_STEP_OIDC_TOKEN': 'token', 'AWS_OIDC_ROLE_ARN': 'account/role',
                                  'AWS_DEFAULT_REGION': 'test-region'})
    def test_discover_auth_method_oidc_only(self, mock_logger):
        from pipe import pipe

        with open(os.path.join(os.getcwd(), 'pipe.yml')) as f:
            metadata = yaml.safe_load(f.read())
        pipe = pipe.SamDeployPipe(schema=pipe.schema, pipe_metadata=metadata, check_for_newer_version=True)
        pipe.auth()

        mock_logger.assert_called_with('Authenticating with a OpenID Connect (OIDC) Web Identity Provider.')
        self.assertEqual(pipe.auth_method, 'OIDC_AUTH')
        self.assertTrue(os.path.exists(f'{os.environ["HOME"]}/.aws/.aws-oidc'))
        config = configparser.ConfigParser()
        config.read(f'{os.environ["HOME"]}/.aws/config')
        self.assertEqual(config['default'].get('role_arn'), 'account/role')
        token_file = config['default'].get('web_identity_token_file')

        with open(token_file) as f:
            self.assertEqual(f.read(), 'token')

    @mock.patch('pipe.pipe.logger.info')
    @mock.patch.dict(os.environ, {'BITBUCKET_STEP_OIDC_TOKEN': 'token', 'AWS_OIDC_ROLE_ARN': 'account/role',
                                  'AWS_ACCESS_KEY_ID': 'akiafkae', 'AWS_SECRET_ACCESS_KEY': 'secretkey',
                                  'AWS_DEFAULT_REGION': 'test-region'})
    def test_discover_auth_method_oidc_and_default_credentials(self, mock_logger):
        from pipe import pipe

        with open(os.path.join(os.getcwd(), 'pipe.yml')) as f:
            metadata = yaml.safe_load(f.read())

        self.assertTrue('AWS_ACCESS_KEY_ID' in os.environ)
        self.assertTrue('AWS_SECRET_ACCESS_KEY' in os.environ)
        pipe = pipe.SamDeployPipe(schema=pipe.schema, pipe_metadata=metadata, check_for_newer_version=True)
        pipe.auth()

        mock_logger.assert_called_with('Authenticating with a OpenID Connect (OIDC) Web Identity Provider.')

        self.assertEqual(pipe.auth_method, 'OIDC_AUTH')
        self.assertTrue(os.path.exists(f'{os.environ["HOME"]}/.aws/.aws-oidc'))
        config = configparser.ConfigParser()
        config.read(f'{os.environ["HOME"]}/.aws/config')
        self.assertEqual(config['default'].get('role_arn'), 'account/role')
        token_file = config['default'].get('web_identity_token_file')

        with open(token_file) as f:
            self.assertEqual(f.read(), 'token')

        self.assertFalse('AWS_ACCESS_KEY_ID' in os.environ)
        self.assertFalse('AWS_SECRET_ACCESS_KEY' in os.environ)

    @mock.patch('pipe.pipe.logger.info')
    @mock.patch('pipe.pipe.logger.warning')
    @mock.patch.dict(os.environ, {'BITBUCKET_STEP_OIDC_TOKEN': '', 'AWS_OIDC_ROLE_ARN': 'account/role',
                                  'AWS_ACCESS_KEY_ID': 'akiafkae', 'AWS_SECRET_ACCESS_KEY': 'secretkey',
                                  'AWS_DEFAULT_REGION': 'test-region'})
    def test_discover_auth_method_fallback_to_default_credentials(self, mock_warning, mock_info):
        from pipe import pipe

        with open(os.path.join(os.getcwd(), 'pipe.yml')) as f:
            metadata = yaml.safe_load(f.read())

        pipe = pipe.SamDeployPipe(schema=pipe.schema, pipe_metadata=metadata, check_for_newer_version=True)
        pipe.auth()

        mock_info.assert_called_with('Using default authentication with AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY.')
        mock_warning.assert_called_with('Parameter `oidc: true` in the step configuration '
                                        'is required for OIDC authentication')

        self.assertEqual(pipe.auth_method, 'DEFAULT_AUTH')
        base_config_dir = f'{os.environ["HOME"]}/.aws'

        self.assertFalse(os.path.exists(f'{base_config_dir}/.aws-oidc'))
