import configparser
import os
import stat
import subprocess
import time

import yaml

from bitbucket_pipes_toolkit import Pipe, get_logger


MSG_AWS_SAM_PACKAGE_FAILED = 'Failed SAM package.'
DEFAULT_SAM_TEMPLATE_FILENAME = 'template.yaml'


schema_commands = ("deploy", "package")

schema = {
    "AWS_ACCESS_KEY_ID": {
        "type": "string",
        "required": True
    },
    "AWS_SECRET_ACCESS_KEY": {
        "type": "string",
        "required": True
    },
    "AWS_DEFAULT_REGION": {
        "type": "string",
        "required": True
    },
    "COMMAND": {
        "type": "string",
        "allowed": schema_commands,
        "default": schema_commands[0]
    },
    'EXTRA_OPTIONS_DEPLOY': {
        'type': 'list',
        'required': False,
        'default': []
    },
    'EXTRA_OPTIONS_PACKAGE': {
        'type': 'list',
        'required': False,
        'default': []
    },
    'DEBUG': {
        "type": "boolean",
        "default": False,
        "required": False
    }
}

logger = get_logger()


class SamDeployPipe(Pipe):
    OIDC_AUTH = 'OIDC_AUTH'
    DEFAULT_AUTH = 'DEFAULT_AUTH'

    def __init__(self, pipe_metadata=None, schema=None, env=None, check_for_newer_version=False):
        self.auth_method = self.discover_auth_method()
        super().__init__(pipe_metadata=pipe_metadata, schema=schema, env=env,
                         check_for_newer_version=check_for_newer_version)
        self.debug = self.get_variable('DEBUG')
        self.command = self.get_variable('COMMAND')

    def discover_auth_method(self):
        """Discover user intentions: authenticate to AWS through OIDC or default aws access keys"""
        oidc_role = os.getenv('AWS_OIDC_ROLE_ARN')
        web_identity_token = os.getenv('BITBUCKET_STEP_OIDC_TOKEN')
        if oidc_role:
            if web_identity_token:
                logger.info('Authenticating with a OpenID Connect (OIDC) Web Identity Provider.')
                schema['BITBUCKET_STEP_OIDC_TOKEN'] = {'required': True}
                schema['AWS_OIDC_ROLE_ARN'] = {'required': True}

                schema['AWS_ACCESS_KEY_ID']['required'] = False
                schema['AWS_SECRET_ACCESS_KEY']['required'] = False

                os.environ.pop('AWS_ACCESS_KEY_ID', None)
                os.environ.pop('AWS_SECRET_ACCESS_KEY', None)

                return self.OIDC_AUTH

            logger.warning('Parameter `oidc: true` in the step configuration is required for OIDC authentication')
            logger.info('Using default authentication with AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY.')
            return self.DEFAULT_AUTH

        logger.info('Using default authentication with AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY.')
        return self.DEFAULT_AUTH

    def auth(self):
        """Authenticate via chosen method"""
        if self.auth_method == self.OIDC_AUTH:
            random_number = str(time.time_ns())
            aws_config_directory = os.path.join(os.environ["HOME"], '.aws')
            oidc_token_directory = os.path.join(aws_config_directory, '.aws-oidc')

            os.makedirs(aws_config_directory, exist_ok=True)
            os.makedirs(oidc_token_directory, exist_ok=True)

            web_identity_token_path = os.path.join(oidc_token_directory, f'oidc_token_{random_number}')
            with open(web_identity_token_path, 'w') as f:
                f.write(self.get_variable('BITBUCKET_STEP_OIDC_TOKEN'))

            os.chmod(web_identity_token_path, mode=stat.S_IRUSR)
            logger.debug('Web identity token file is created')

            aws_configfile_path = os.path.join(aws_config_directory, 'config')
            with open(aws_configfile_path, 'w') as configfile:
                config = configparser.ConfigParser()
                config['default'] = {
                    'role_arn': self.get_variable('AWS_OIDC_ROLE_ARN'),
                    'web_identity_token_file': web_identity_token_path
                }
                config.write(configfile)
            logger.debug('Configured settings for authentication with assume web identity role')

    def package(self):
        """
        Packages the local artifacts (local paths) that your AWS SAM template references.
        The command uploads local artifacts, such as source code for AWS Lambda functions, to an S3 bucket.
        https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/sam-cli-command-reference-sam-package.html
        """
        logger.info('Start package...')
        args = ["sam", "package"]
        args.extend(list(self.get_variable('EXTRA_OPTIONS_PACKAGE')))

        if self.debug:
            args.append('--debug')

        logger.info(args)

        try:
            process = subprocess.run(
                args,
                check=True,
                text=True,
                capture_output=True,
                encoding="utf-8"
            )
        except subprocess.CalledProcessError as e:
            self.fail(f'Pipe has finished with an error: {e.returncode}, {e.output} {e.stderr}')

        logger.info(process.stdout)

    def deploy(self):
        """
        Deploys an AWS SAM application.
        https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/sam-cli-command-reference-sam-deploy.html
        sam deploy now implicitly performs the functionality of sam package. You can use the sam deploy command directly
        to package and deploy your application.
        """
        logger.info('Start deploy...')
        args = ["sam", "deploy"]
        args.extend(list(self.get_variable('EXTRA_OPTIONS_DEPLOY')))
        args.extend(['--no-confirm-changeset', '--no-fail-on-empty-changeset'])

        if self.debug:
            args.append('--debug')

        logger.info(args)

        try:
            process = subprocess.run(
                args,
                check=True,
                text=True,
                capture_output=True,
                encoding="utf-8"
            )
        except subprocess.CalledProcessError as e:
            self.fail(f'Pipe has finished with an error: {e.returncode}, {e.output} {e.stderr}')

        logger.info(process.stdout)

    def run(self):
        self.auth()

        if self.command == "package":
            # build artifacts packaged_template and upload zipped app to AWS S3
            self.package()
        else:
            # deploy app via aws sam cli
            self.deploy()

        self.success('Pipe has finished successfully.')


if __name__ == '__main__':
    with open('/pipe.yml', 'r') as metadata_file:
        metadata = yaml.safe_load(metadata_file.read())
    pipe = SamDeployPipe(schema=schema, pipe_metadata=metadata, check_for_newer_version=True)
    pipe.run()
