# Bitbucket Pipelines Pipe: AWS SAM deploy

Deploy an AWS serverless lambda stack using [AWS Serverless Application Model (AWS SAM)](https://aws.amazon.com/serverless/sam/).


## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:
    
```yaml
- pipe: atlassian/aws-sam-deploy:2.4.1
  variables:
    AWS_ACCESS_KEY_ID: '<string>' # Optional if already defined in the context.
    AWS_SECRET_ACCESS_KEY: '<string>' # Optional if already defined in the context.
    AWS_DEFAULT_REGION: '<string>' # Optional if already defined in the context.
    AWS_OIDC_ROLE_ARN: '<string>' # Optional by default. Required for OpenID Connect (OIDC) authentication.
    # COMMAND: '<string>' # Optional. `package` or `deploy`. Default: `deploy`.
    # EXTRA_OPTIONS_DEPLOY: '<list>' of strings # Optional. Allows to use options for deploy command.
    # EXTRA_OPTIONS_PACKAGE: '<list>' of strings # Optional. Allows to use options for package command.
    # DEBUG: '<boolean>' # Optional.
```


## Authentication

Supported options:

1. Environment variables: AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY. Default option.

2. Assume role provider with OpenID Connect (OIDC). More details in the Bitbucket Pipelines Using OpenID Connect guide [Integrating aws bitbucket pipeline with oidc][aws-oidc]. Make sure that you set up OIDC before:
    * configure Bitbucket Pipelines as a Web Identity Provider in AWS
    * attach to provider your AWS role with required policies in AWS
    * set up a build step with `oidc: true` in your Bitbucket Pipelines
    * pass AWS_OIDC_ROLE_ARN (*) variable that represents role having appropriate permissions to execute actions on SAM resources


## Variables

| Variable                   | Usage                                                                                                                                                                                                                                                                                                                                                                                             |
|----------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| AWS_ACCESS_KEY_ID (**)     | AWS access key.                                                                                                                                                                                                                                                                                                                                                                                   |
| AWS_SECRET_ACCESS_KEY (**) | AWS secret key.                                                                                                                                                                                                                                                                                                                                                                                   |
| AWS_DEFAULT_REGION (**)    | The AWS region code (us-east-1, us-west-2, etc.) of the region containing the AWS resource(s). For more information, see [Regions and Endpoints][Regions and Endpoints] in the _Amazon Web Services General Reference_.                                                                                                                                                                           |
| AWS_OIDC_ROLE_ARN          | The ARN of the role used for web identity federation or OIDC. See **Authentication**.                                                                                                                                                                                                                                                                                                             |
| COMMAND                    | Command to be executed during the deployment. Valid options are `package`, `deploy`. Default: `deploy`.                                                                                                                                                                                                                                                                                           |
| EXTRA_OPTIONS_DEPLOY       | Options you can provide for sam `deploy` command in format `['--option1=value1', '--option2', 'value2']`. Available options for [deploy][aws sam deploy]. `--debug` option will be added automatically if `DEBUG` is set to `true`. `--no-confirm-changeset` and  `--no-fail-on-empty-changeset` will be added automatically according to [AWS Bitbucket SAM example][aws sam bitbucket example]. |
| EXTRA_OPTIONS_PACKAGE      | Options you can provide for sam `package` command in format `['--option1=value1', '--option2', 'value2']`. Available options for [package][aws sam package]. `--debug` option will be added automatically if `DEBUG` is set to `true`.                                                                                                                                                            |
| DEBUG                      | Turn on extra debug information. Default: `false`.                                                                                                                                                                                                                                                                                                                                                |
_(*) = required variable. This variable needs to be specified always when using the pipe._
_(**) = required variable. If this variable is configured as a repository, account or environment variable, it doesn’t need to be declared in the pipe as it will be taken from the context. It can still be overridden when using the pipe._


## Details

The AWS Serverless Application Model (SAM) is an open-source framework for building serverless applications. It provides shorthand syntax to express functions, APIs, databases, and event source mappings. With just a few lines per resource, you can define the application you want and model it using YAML. During deployment, SAM transforms and expands the SAM syntax into AWS CloudFormation syntax, enabling you to build serverless applications faster.

By default, when you use package or deploy command, the AWS SAM CLI assumes that your current working directory is your project's root directory. The AWS SAM CLI first tries to locate a template file built using the [sam build][aws sam build] command, located in the .aws-sam subfolder, and named template.yaml. Next, the AWS SAM CLI tries to locate a template file named template.yaml or template.yml in the current working directory. If you specify the --template option, AWS SAM CLI's default behavior is overridden, and will package or deploy just that AWS SAM template and the local resources it points to.

We recommend using the `COMMAND` variable to separate your CI/CD workflow into different operations:

- `COMMAND: 'package'`: It will [package][aws sam package] an AWS SAM application. The command uploads local artifacts, such as source code for an AWS Lambda function, to an S3 bucket.
- `COMMAND: 'deploy'`: It will [deploy][aws sam deploy] your configuration as code. According to [aws sam package note][aws sam package] `deploy` now implicitly performs the functionality of `package`. You can use the `deploy` command directly to package and deploy your application.


## Prerequisites
* An IAM user is configured with sufficient permissions to perform a deployment of your application using CloudFormation.
* You have configured the AWS S3 bucket.
* CloudFormation templates greater than 51200 bytes must be deployed from an AWS S3 bucket by providing AWS S3 URL. For further details please see [AWS CloudFormation limits][AWS CloudFormation limits].


## Examples 

### Basic example:

Deploy a new version of your AWS SAM stack, using default template file named template.yaml or template.yml located in your repository.

```yaml
script:
  - pipe: atlassian/aws-sam-deploy:2.4.1
    variables:
      AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
      AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
      AWS_DEFAULT_REGION: 'us-east-1'
      EXTRA_OPTIONS_DEPLOY: ['--s3-bucket=my-s3-bucket', '--stack-name=my-stack-name']
```

Deploy a new version of your AWS SAM stack, using a default template file named template.yaml or template.yml located in your repository. `AWS_ACCESS_KEY_ID`, `AWS_SECRET_ACCESS_KEY` and `AWS_DEFAULT_REGION` are configured as repository variables, so there is no need to declare them in the pipe.

```yaml
script:
  - pipe: atlassian/aws-sam-deploy:2.4.1
    variables:
      EXTRA_OPTIONS_DEPLOY: ['--s3-bucket=my-s3-bucket', '--stack-name=my-stack-name']
```

Package a new version of your AWS SAM stack, using default template file named template.yaml or template.yml located in your repository. `AWS_ACCESS_KEY_ID`, `AWS_SECRET_ACCESS_KEY` and `AWS_DEFAULT_REGION` are configured as repository variables, so there is no need to declare them in the pipe.

```yaml
script:
  - pipe: atlassian/aws-sam-deploy:2.4.1
    variables:
      COMMAND: 'package'
      EXTRA_OPTIONS_PACKAGE: ['--s3-bucket=my-s3-bucket']
```

### Advanced example:

Deploy a new version of your CloudFormation stack with OpenID Connect (OIDC) alternative authentication without required `AWS_ACCESS_KEY_ID`, `AWS_SECRET_ACCESS_KEY`.
Parameter `oidc: true` in the step configuration and variable `AWS_OIDC_ROLE_ARN` are required.

```yaml

- step:
    oidc: true
    script:
      - pipe: atlassian/aws-sam-deploy:2.4.1
        variables:
          AWS_DEFAULT_REGION: $AWS_DEFAULT_REGION
          AWS_OIDC_ROLE_ARN: 'arn:aws:iam::123456789012:role/role_name'
          EXTRA_OPTIONS_DEPLOY: ['--s3-bucket=my-s3-bucket', '--stack-name=my-stack-name']
```

Deploy an AWS SAM application with [capabilities][capabilities] extra options.
```yaml
script:
  - pipe: atlassian/aws-sam-deploy:2.4.1
    variables:
      AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
      AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
      AWS_DEFAULT_REGION: 'us-east-1'
      EXTRA_OPTIONS_DEPLOY: ['--s3-bucket=my-s3-bucket', '--stack-name=my-stack-name', '--template=template.yaml', '--capabilities', 'CAPABILITY_IAM', 'CAPABILITY_AUTO_EXPAND']
```

If you want to package or deploy sam application with more advanced settings, 
create file with `toml` extension in the root of sam application directory and pass it to the pipe.
Follow [AWS SAM configuration file][aws-sam-configuration-toml]
docs to construct config.toml file. 
Example of samconfig.toml:

```
version=0.1
[default.package.parameters]
s3_prefix="my-s3-prefix"
```
When you use the default config file and specify --template-file SAM CLI expects samconfig.toml and the template file to be in the same directory.
Alternatively, if --config-file is explicitly specified, it can point to a custom samconfig.toml location.
Keep in mind SAM CLI will start looking for a config file from root dir, which is the directory where your template file presented.
Also note about [precedence][aws sam precedence] when providing the same variables in configuration file and options.

Package and deploy a new version of your AWS SAM application with AWS SAM CLI config file.

```yaml
script:
  - pipe: atlassian/aws-sam-deploy:2.4.1
    variables:
      AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
      AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
      AWS_DEFAULT_REGION: 'us-east-1'
        EXTRA_OPTIONS_DEPLOY: ['--template=template.yaml', '--config-file=samconfig.toml']
```

Package and deploy a new version of your AWS SAM application with AWS SAM CLI config file from different directory.

```markdown
├── sam-app
│     └── template.yaml
└── config.toml
```

```yaml
script:
  - pipe: atlassian/aws-sam-deploy:2.4.1
    variables:
      AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
      AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
      AWS_DEFAULT_REGION: 'us-east-1'
        EXTRA_OPTIONS_DEPLOY: ['--template=sam-app/template.yaml', '--config-file=../config.toml']
```

Package and deploy a new version of your AWS SAM application with `PackageType: Image` in `template.yaml` located in i.e. `sam-app` folder. For more details check [using container image support for AWS Lambda with AWS SAM guide][aws deploy image].
The image name should be the same as your ECR repository name (example: $AWS_ACCOUNT_NAME/my-ecr-image).
To not expose your aws account name in bitbucket-pipelines.yml config file, store it under Bitbucket repository secret variables i.e. `5555555555.dkr.ecr.us-east-1.amazonaws.com`.

Note! Keep in mind your AWS-ACCOUNT_NAME will still be visible in pipeline logs output when aws sam cli command will be executed.

Set up `ImageUri: helloworldfunction:python3.8-v1` in `template.yaml`. Build your docker image located in `sam-app` folder. Run the pipe.

```yaml
script:
  - docker build -t helloworldfunction:python3.8-v1 <path-to-sam-app-dockerfile>
  - pipe: atlassian/aws-sam-deploy:2.4.1
    variables:
      AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
      AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
      AWS_DEFAULT_REGION: 'us-east-1'
        EXTRA_OPTIONS_DEPLOY: ['--template=sam-app/template.yaml', '--image-repository=$AWS_ACCOUNT_NAME/my-ecr-image']
```


## Support
If you’d like help with this pipe, or you have an issue or feature request, [let us know on Community][community].

If you’re reporting an issue, please include:

* the version of the pipe
* relevant logs and error messages
* steps to reproduce


## License
Copyright (c) 2018 Atlassian and others.
Apache 2.0 licensed, see [LICENSE.txt](LICENSE.txt) file.

[community]: https://community.atlassian.com/t5/forums/postpage/board-id/bitbucket-pipelines-questions?add-tags=pipes,aws,serverless
[capabilities]: https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/using-iam-template.html#using-iam-capabilities
[aws-oidc]: https://support.atlassian.com/bitbucket-cloud/docs/deploy-on-aws-using-bitbucket-pipelines-openid-connect
[AWS CloudFormation limits]: https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/cloudformation-limits.html
[aws-sam-configuration-toml]: https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/serverless-sam-cli-config.html
[aws sam build]: https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/sam-cli-command-reference-sam-build.html
[aws sam package]: https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/sam-cli-command-reference-sam-package.html
[aws sam deploy]: https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/sam-cli-command-reference-sam-deploy.html
[aws sam bitbucket example]: https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/deploying-using-bitbucket.html
[aws sam precedence]: https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/serverless-sam-cli-config.html#rules-precedence
[aws sam deploy image]: https://aws.amazon.com/blogs/compute/using-container-image-support-for-aws-lambda-with-aws-sam/
[Regions and Endpoints]: https://docs.aws.amazon.com/general/latest/gr/rande.html
