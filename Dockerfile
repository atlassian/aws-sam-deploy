FROM python:3.10

SHELL ["/bin/bash", "-o", "pipefail", "-c"]

RUN wget --no-verbose https://github.com/aws/aws-sam-cli/releases/download/v1.108.0/aws-sam-cli-linux-x86_64.zip && \
       echo 'd7376b819ae0e703336af7ea3ba08563b930b1f747df2dda3ff0a1a881d68533 aws-sam-cli-linux-x86_64.zip' | sha256sum -c - && \
       unzip aws-sam-cli-linux-x86_64.zip -d sam-installation && ./sam-installation/install

# install requirements
COPY requirements.txt /
WORKDIR /
RUN pip install --no-cache-dir -r requirements.txt

# copy the pipe source code
COPY pipe /
COPY pipe.yml LICENSE.txt README.md /


ENTRYPOINT ["python3", "/pipe.py"]
