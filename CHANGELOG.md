# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 2.4.1

- patch: Internal maintenance: Bump release pipe version. Add multi-platform build: linux/arm64 and linux/amd64.

## 2.4.0

- minor: Bump aws sam cli to 1.108.0.
- patch: Internal maintenance: update pipes versions in pipelines config file.

## 2.3.0

- minor: Bump bitbucket-pipes-toolkit to fix vulnerabilities.

## 2.2.0

- minor: Bump bitbucket-pipes-toolkit to fix vulnerabilities.

## 2.1.0

- minor: Update bitbucket-pipes-toolkit to fix vulnerability with certify.

## 2.0.0

- major: Refactor pipe to use aws sam cli instead of boto3. Now the pipe natively supports aws sam cli and more flexible in configuration.
- minor: Bump aws sam cli to 1.66.0.
- minor: README updated with info about variable precedence.
- patch: Added to README example of deploy using container image support.
- patch: Fix AWS_DEFAULT_REGION variable typo in README.
- patch: Internal maintenance: Update Dockerfile.
- patch: Internal maintenance: Update community link.
- patch: Internal maintenance: Update release process.
- patch: Internal maintenance: update bitbucket-pipelines.yml upgrade pipes versions and add secret scan check.

## 1.5.0

- minor: Bump AWS SAM cli to 1.36.0 version.

## 1.4.0

- minor: Bump version of aws-cloudformation-deploy to 0.13.0 and bitbucket-pipes-toolkit to 3.2.1

## 1.3.1

- patch: Internal maintenance: Bump dependency aws-cloudformation-deploy pipe base package.
- patch: Internal maintenance: Fix dependency package.

## 1.3.0

- minor: Bump aws-sam-cli to 1.27.2 version.

## 1.2.0

- minor: Support AWS SAM config on sam package.
- patch: Bugfix: fix DEBUG option for package

## 1.1.0

- minor: Support AWS OIDC authentication. Environment variables AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY are not required anymore.

## 1.0.0

- major: AWS SAM cli -> 1.18.0. Replace with aws sam cli official binary.

## 0.5.2

- patch: Internal maintenance: fix README: change aws-s3-deploy tag back.

## 0.5.1

- patch: Internal maintenance: bump bitbucket-pipe-release.

## 0.5.0

- minor: Internal maintenance: bump bitbucket-pipes-toolkit version.

## 0.4.0

- minor: Bump aws-sam-cli 0.17.0 -> 0.53.0.
- patch: Bugfix: Separate required variables for package-only and deploy-only.

## 0.3.4

- patch: Internal maintenance: change pipe metadata according to new structure

## 0.3.3

- patch: Internal maintenance: Add gitignore secrets.

## 0.3.2

- patch: Internal maintenance: Add auto infrastructure for tests.

## 0.3.1

- patch: Update the Readme with a new Atlassian Community link.
- patch: Update the Readme with details about default variables.

## 0.3.0

- minor: Add default values for AWS variables.

## 0.2.5

- patch: Internal maintenance: Add hadolint linter for Dockerfile

## 0.2.4

- patch: Add warning message about new version of the pipe available.

## 0.2.3

- patch: Updated pipes toolkit version to fix coloring of log info messages.

## 0.2.2

- patch: Internal release

## 0.2.1

- patch: Updated readme with templates examples.

## 0.2.0

- minor: Add support for custom packaged template name.

## 0.1.3

- patch: Update success message with create or update.

## 0.1.2

- patch: Internal maintenance: Update Readme.

## 0.1.1

- patch: Internal maintenance: update pipes toolkit version.

## 0.1.0

- minor: Initial release.
